//    ______
//   /_  __/___  ____ ___  ___  ____
//    / / / __ \/ __ `__ \/ _ \/ __ \
//   / / / /_/ / / / / / /  __/ /_/ /
//  /_/  \____/_/ /_/ /_/\___/\____/
//              video for sports enthusiasts...
//
//  2811 cw3 : twak 11/11/2021
//

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QScrollArea>
#include "the_player.h"
#include "the_button.h"
#include "control_button.h"
#include "queue_button.h"
#include "qarea.h"
#include <QLabel>
#include <QToolButton>
#include <QSlider>
#include <QStyle>
#include <QFont>

// read in videos and thumbnails to this directory
std::vector<TheButtonInfo> getInfoIn (std::string loc) {

    std::vector<TheButtonInfo> out =  std::vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder
    std::vector<TheButtonInfo> videos;

    if (argc == 2)
        videos = getInfoIn( std::string(argv[1]) );

    if (videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }

    QWidget *social = new QWidget();
    social->setGeometry(0, 0, 100, 100);
    QVBoxLayout *sociallayout = new QVBoxLayout();
    QLabel *socialLabel = new QLabel();
    socialLabel->setText("This links to a social media API for sharing your videos");
    social->setLayout(sociallayout);
    sociallayout->addWidget(socialLabel);

    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;
    videoWidget->setStyleSheet("background-color: black");
    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    QScrollArea *ButtonScroller = new QScrollArea();
    ButtonScroller->setMaximumWidth(250);
    ButtonScroller->setMinimumWidth(250);
    QLabel  *liblabel = new QLabel;
    liblabel->setText("Video Library");
    QFont font2 = liblabel->font();
    font2.setPointSize(12);
    font2.setBold(true);
    liblabel->setFont(font2);
    // a row of buttons
    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    std::vector<TheButton*> buttons;
    // the buttons are arranged horizontally
    QVBoxLayout *layout = new QVBoxLayout();
    buttonWidget->setLayout(layout);
    QArea *queuelayout = new QArea(NULL);
    queuelayout->connect(player, SIGNAL(removewidget()), queuelayout, SLOT(removewidget()));

    // create the four buttons
    for ( int i = 0; i < int(videos.size()); i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        QHBoxLayout *vlabel = new QHBoxLayout();
        QButton *qbtn = new QButton(buttonWidget);
        QLabel *label = new QLabel();
        label->setText((&videos.at(i))->url->fileName().left((&videos.at(i))->url->fileName().length() - 4));
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        qbtn->connect(qbtn, SIGNAL(addQ(TheButtonInfo*)), player, SLOT(addQ(TheButtonInfo*)));
        qbtn->connect(qbtn, SIGNAL(addwidget(TheButtonInfo*)), queuelayout, SLOT(addwidget(TheButtonInfo*)));
        qbtn->setText("Queue Video");
        buttons.push_back(button);
        vlabel->addWidget(label);
        vlabel->addWidget(qbtn);
        layout->addWidget(button);
        layout->addLayout(vlabel);
        button->init(&videos.at(i));
        qbtn->initQ(&videos.at(i));
    }
    ButtonScroller->setWidget(buttonWidget);


    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    //create video control buttons
    QLabel *videoTitle = new QLabel();
    videoTitle->connect(player, &QMediaPlayer::mediaChanged, videoTitle, [&] {
        videoTitle->setText(player->media().canonicalUrl().fileName().left(player->media().canonicalUrl().fileName().length()-4));
    });
    videoTitle->setFixedHeight(20);
    videoTitle->setAlignment(Qt::AlignHCenter);

    // video control layout
    QVBoxLayout *controls = new QVBoxLayout;

    // video slider
    QSlider *slider = new QSlider();
    // set slider's maximum to video's duration
    slider->connect(player, &QMediaPlayer::durationChanged, slider, [&](qint64 dur) {
        slider->setMaximum(dur);
    });
    // set slider to current position in the video
    slider->connect(player, &QMediaPlayer::positionChanged, slider, [&](qint64 dur) {
        slider->setValue(dur);
    });
    // change position in video when slider is moved
    slider->connect(slider, &QSlider::sliderMoved, player, [&](qint64 dur) {
        player->setPosition(dur);
    });
    slider->setOrientation(Qt::Horizontal);
    slider->setFixedHeight(20);
    controls->addWidget(slider);

    QHBoxLayout *playbackAndSound = new QHBoxLayout();
    controls->addLayout(playbackAndSound);

    // play/pause button
    QToolButton *playButton = new QToolButton();
    playButton->setGeometry(0,0,40,40);
    playbackAndSound->addWidget(playButton);
    playButton->setFixedSize(40,40);
    playButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
    // button icon updates
    player->connect(player, &QMediaPlayer::stateChanged, playButton, [&] {
        if(player->state()==QMediaPlayer::PlayingState) playButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
        else playButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));

        //if(player->state()==QMediaPlayer::StoppedState) player->setMedia(0);
    });
    // button behaviour updates
    playButton->connect(playButton, &QToolButton::released, player, [&] {
        if(player->state()==QMediaPlayer::PlayingState) player->pause();
        else if(player->state()==QMediaPlayer::PausedState) player->play();
    });

    QToolButton *stopButton = new QToolButton();
    stopButton->setGeometry(0,0,40,40);
    playbackAndSound->addWidget(stopButton);
    stopButton->setFixedSize(40,40);
    stopButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaStop));
    stopButton->connect(stopButton, &QToolButton::released, player, [&] {
        player->stop();
    });

    playbackAndSound->addStretch();

    // sound control image
    QLabel *soundIcon = new QLabel();
    soundIcon->setGeometry(0,0,40,40);
    soundIcon->setFixedSize(40,40);
    soundIcon->setAlignment(Qt::AlignCenter);
    soundIcon->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MediaVolume).pixmap(QSize(40,40)));
    playbackAndSound->addWidget(soundIcon);

    // sound control slider
    QSlider *soundSlider = new QSlider();
    soundSlider->setMinimum(0);
    soundSlider->setMaximum(100);
    soundSlider->setOrientation(Qt::Horizontal);
    soundSlider->setFixedSize(100,20);
    soundSlider->setValue(50);
    soundSlider->connect(soundSlider, &QSlider::valueChanged, player, [&](qint64 dur) {
        player->setVolume(dur);
    });
    playbackAndSound->addWidget(soundSlider);

    QWidget *helpWindow = new QWidget();
    helpWindow->setWindowTitle("Help");
    helpWindow->setMinimumSize(600,400);
    helpWindow->setMaximumSize(600,400);

    QGridLayout *helpLayout = new QGridLayout();
    helpLayout->setColumnStretch(3,1);


    QLabel *playbackTitle = new QLabel("Video playback controls");
    playbackTitle->setFont(font2);
    playbackTitle->setFixedHeight(20);
    //playbackTitle->setAlignment(Qt::AlignLeft);
    helpLayout->addWidget(playbackTitle,0,0,Qt::AlignLeft);

    QSlider *playbackHelpSlider = new QSlider();
    playbackHelpSlider->setFixedSize(200,20);
    playbackHelpSlider->setOrientation(Qt::Horizontal);
    helpLayout->addWidget(playbackHelpSlider,1,0,Qt::AlignLeft);
    QLabel *playbackSliderLabel = new QLabel("Click in the slider to go to another part of the video");
    helpLayout->addWidget(playbackSliderLabel,1,1);

    QHBoxLayout *playPauseLayout = new QHBoxLayout();
    QToolButton *playHelpButton = new QToolButton();
    playHelpButton->setGeometry(0,0,40,40);
    playHelpButton->setFixedSize(40,40);
    playHelpButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
    playPauseLayout->addWidget(playHelpButton);
    QToolButton *pauseHelpButton = new QToolButton();
    pauseHelpButton->setGeometry(0,0,40,40);
    pauseHelpButton->setFixedSize(40,40);
    pauseHelpButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
    playPauseLayout->addWidget(pauseHelpButton);
    playPauseLayout->addStretch();
    helpLayout->addLayout(playPauseLayout,2,0);
    QLabel *playPauseLabel = new QLabel("Click to play/pause the video");
    helpLayout->addWidget(playPauseLabel,2,1);

    QToolButton *stopHelpButton = new QToolButton();
    stopHelpButton->setGeometry(0,0,40,40);
    stopHelpButton->setFixedSize(40,40);
    stopHelpButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaStop));
    helpLayout->addWidget(stopHelpButton,3,0);
    QLabel *stopLabel = new QLabel("Click to stop the video");
    helpLayout->addWidget(stopLabel,3,1);

    QHBoxLayout *soundHelpLayout = new QHBoxLayout();
    QLabel *soundHelpIcon = new QLabel();
    soundHelpIcon->setGeometry(0,0,40,40);
    soundHelpIcon->setFixedSize(20,20);
    soundHelpIcon->setAlignment(Qt::AlignCenter);
    soundHelpIcon->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MediaVolume).pixmap(QSize(40,40)));
    soundHelpLayout->addWidget(soundHelpIcon);
    QSlider *soundHelpSlider = new QSlider();
    soundHelpSlider->setFixedSize(100,20);
    soundHelpSlider->setOrientation(Qt::Horizontal);
    soundHelpLayout->addWidget(soundHelpSlider);
    soundHelpLayout->addStretch();
    helpLayout->addLayout(soundHelpLayout,4,0);
    QLabel *soundHelpLabel = new QLabel("Click in the slider to change the volume of the video");
    helpLayout->addWidget(soundHelpLabel,4,1);
    QLabel *videoSelectionLabel = new QLabel("Video selection");
    videoSelectionLabel->setFont(font2);
    videoSelectionLabel->setFixedHeight(40);
    helpLayout->addWidget(videoSelectionLabel,5,0);

    QHBoxLayout *helperVideoLayout = new QHBoxLayout();
    QPixmap pix1((&videos.at(0))->url->fileName());
    QLabel *video1Icon = new QLabel();
    video1Icon->setGeometry(0,0,200,110);
    video1Icon->setFixedSize(100,55);
    video1Icon->setAlignment(Qt::AlignCenter);
    video1Icon->setPixmap(videos.at(0).icon->pixmap(QSize(100,55)));
    helperVideoLayout->addWidget(video1Icon);
    QPixmap pix2((&videos.at(1))->url->fileName());
    pix2.scaled(100, 55);
    QLabel *video2Icon = new QLabel();
    video2Icon->setGeometry(0,0,40,40);
    video2Icon->setFixedSize(100,55);
    video2Icon->setAlignment(Qt::AlignCenter);
    video2Icon->setPixmap(videos.at(1).icon->pixmap(QSize(100,55)));
    helperVideoLayout->addWidget(video2Icon);
    helpLayout->addLayout(helperVideoLayout,6,0);
    QLabel *videoHelpLabel = new QLabel("Click to play corresponding video");
    videoHelpLabel->setFixedHeight(40);
    helpLayout->addWidget(videoHelpLabel,6,1);

    QPushButton *queueHelpButton = new QPushButton();
    queueHelpButton->setGeometry(0,0,80,40);
    queueHelpButton->setFixedSize(80,40);
    queueHelpButton->setText("Queue Video");
    helpLayout->addWidget(queueHelpButton,8,0);
    QLabel *queueLabel = new QLabel("Click to queue the corresponding video");
    helpLayout->addWidget(queueLabel,8,1);

    helpWindow->setLayout(helpLayout);

    //---------------------------------------------

    QToolButton *helpButton = new QToolButton();
    helpButton->setGeometry(0,0,20,20);
    helpButton->setFixedSize(20,20);
    helpButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_TitleBarContextHelpButton));
    // make help button round
    helpButton->setStyleSheet("background-color: white; border-style: solid; border-width:1px; border-radius:10px; border-color: black;");
    helpButton->connect(helpButton, &QToolButton::released, helpWindow, [&] {
        helpWindow->show();
    });

    //create video queue
    QLabel *qlabel = new QLabel;
    qlabel->setText("Video Queue");
    QFont font = qlabel->font();
    font.setPointSize(12);
    font.setBold(true);
    qlabel->setFont(font);
    queuelayout->setMinimumSize(QSize(videoWidget->width(), 150));
    qlabel->setGeometry(queuelayout->x(), queuelayout->y(), 20, 20);

    //create share control buttons
    QWidget *scontrols = new QWidget();
    QHBoxLayout *top5 = new QHBoxLayout();
    scontrols->setLayout(top5);
    QPushButton *insta = new QPushButton();
    insta->connect(insta, SIGNAL(released()), social, SLOT(show()));
    insta->setText("Instagram");
    top5->addWidget(insta);
    QPushButton *twitter = new QPushButton();
    insta->connect(twitter, SIGNAL(released()), social, SLOT(show()));
    twitter->setText("Twitter");
    top5->addWidget(twitter);
    QPushButton *facebook = new QPushButton();
    insta->connect(facebook, SIGNAL(released()), social, SLOT(show()));
    facebook->setText("Facebook");
    top5->addWidget(facebook);
    QPushButton *email = new QPushButton();
    insta->connect(email, SIGNAL(released()), social, SLOT(show()));
    email->setText("Email");
    top5->addWidget(email);

    // create the main window and layout
    QWidget window;
    QHBoxLayout *top = new QHBoxLayout();
    QVBoxLayout *left = new QVBoxLayout();
    QVBoxLayout *right = new QVBoxLayout();


    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(1000, 680);

    top->addLayout(left);
    top->addLayout(right);
    left->setStretch(100, 10);
    right->stretch(100);
    qlabel->setMaximumHeight(20);
    scontrols->setMaximumHeight(40);
    queuelayout->setMaximumHeight(140);

    QHBoxLayout *helper = new QHBoxLayout();
    helper->addWidget(liblabel);
    helper->addWidget(helpButton);

    // add the video and the buttons to the top level widget
    left->addWidget(videoWidget);
    left->addWidget(videoTitle);
    left->addLayout(controls);
    left->addWidget(scontrols);
    left->addWidget(qlabel);
    left->addWidget(queuelayout);
    right->addLayout(helper);
    right->addWidget(ButtonScroller);

    // showtime!
    window.show();

    if(window.isVisible() == false){
        app.exit();
    }

    // wait for the app to terminate
    return app.exec();
}
