#ifndef LEFTLAYOUT_H
#define LEFTLAYOUT_H


#include <QLayout>

class leftLayout : public QLayout {

public:
    leftLayout(): QLayout() {}
    ~leftLayout();

    // standard functions for a QLayout
    void setGeometry(const QRect &rect);

    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

private:
    QList<QLayoutItem*> list_;
};

#endif // LEFTLAYOUT_H
