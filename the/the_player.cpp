//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <iostream>

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            if(counter >= int(queue.size())){
                counter = 0;
                queue.clear();
                setMedia(0);
            }else{
               jumpTo(queue.at(counter));
               emit removewidget();
               counter++;
            }
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

void ThePlayer::setRate(qreal rate){
    setPlaybackRate(rate);
}

void ThePlayer::addQ(TheButtonInfo* button){
    queue.push_back(button);
}
