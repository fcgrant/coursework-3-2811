#include <QScrollArea>
#include "the_button.h"


#ifndef QUEUE_SCROLLER_H
#define QUEUE_SCROLLER_H


class queue_scroller: public QScrollArea
{
    Q_OBJECT
public:
    queue_scroller(QWidget *parent) :  QScrollArea(parent){};
private slots:
    void addButton(TheButton* button);
signals:
    void add(TheButton* button);
};

#endif // QUEUE_SCROLLER_H
