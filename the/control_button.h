#include <QPushButton>
#ifndef CONTROL_BUTTON_H
#define CONTROL_BUTTON_H


class ControlButton: public QPushButton
{
    Q_OBJECT
public:
    ControlButton(QWidget *parent, qreal rate): QPushButton(parent){

        connect(this, SIGNAL(released()), this, SLOT(clicked()));
        playrate = rate;
    }

    qreal playrate;

private slots:
    void clicked();

signals:
    void setRate(qreal);

};

#endif // CONTROL_BUTTON_H
