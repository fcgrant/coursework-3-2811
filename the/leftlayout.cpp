#include "leftlayout.h"

void leftLayout::setGeometry(const QRect &r ) { // our layout should fit inside r

    QLayout::setGeometry(r);

    // for all the Widgets added in ResponsiveWindow.cpp
    for (int i = 0; i < list_.size(); i++) {

    }
}

// following methods provide a trivial list-based implementation of the QLayout class
int leftLayout::count() const {

    return list_.size();
}

QLayoutItem *leftLayout::itemAt(int idx) const {

    return list_.value(idx);
}

QLayoutItem *leftLayout::takeAt(int idx) {

    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void leftLayout::addItem(QLayoutItem *item) {

    list_.append(item);
}

QSize leftLayout::sizeHint() const {

    return minimumSize();
}

QSize leftLayout::minimumSize() const {

    return QSize(320,320);
}

leftLayout::~leftLayout() {

    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}
