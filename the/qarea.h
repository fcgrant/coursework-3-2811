#include <QWidget>
#include <QHBoxLayout>
#include <QScrollArea>
#include "the_button.h"
#include <vector>

#ifndef QAREA_H
#define QAREA_H


class QArea: public QScrollArea
{
    Q_OBJECT
public:
    QArea(QWidget *parent): QScrollArea(parent){
        this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        frame->setLayout(layout);
        this->setWidget(frame);
        frame->setMinimumSize(QSize(200, 130));
        this->setStyleSheet("background-color: grey");
    };

    int selector = 0;
    std::vector<TheButton*>buttons;
    QHBoxLayout *layout = new QHBoxLayout();
    QWidget *frame = new QWidget();

public slots:
    void addwidget(TheButtonInfo* button);
    void removewidget();
};

#endif // QAREA_H
