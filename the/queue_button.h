#include "the_button.h"
#ifndef QUEUE_BUTTON_H
#define QUEUE_BUTTON_H


class QButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

     QButton(QWidget *parent) :  QPushButton(parent) {
         connect(this, SIGNAL(released()), this, SLOT (clickedQ())); // if QPushButton clicked...then run clicked() below
    }

    void initQ(TheButtonInfo* i);

private slots:
    void clickedQ();
signals:
    void addQ(TheButtonInfo*);

    void addwidget(TheButtonInfo*);

};

#endif // QUEUE_BUTTON_H
